package com.fairytales.wrozkiniebuszki2020;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WrozkiNiebuszki2020Application {

	public static void main(String[] args) {
		SpringApplication.run(WrozkiNiebuszki2020Application.class, args);
	}

}
