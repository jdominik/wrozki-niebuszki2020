package com.fairytales.wrozkiniebuszki2020.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fairytales.wrozkiniebuszki2020.entity.GameRequest;
import com.fairytales.wrozkiniebuszki2020.entity.GameRequest.Status;
import com.fairytales.wrozkiniebuszki2020.entity.Move;
import com.fairytales.wrozkiniebuszki2020.entity.Move.Action;
import com.fairytales.wrozkiniebuszki2020.entity.Move.Direction;
import com.fairytales.wrozkiniebuszki2020.entity.PlayerMove;
import com.fairytales.wrozkiniebuszki2020.manager.NiebuszkowyManager;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(value = "/", method = RequestMethod.POST)
public class GameController {
	
	@GetMapping(path = "/ready", consumes = "application/json", produces ="application/json")
	public FairyResponse test() {
		// for test request - response with playerUID
		return new FairyResponse();
	}
	
	@PostMapping(path = "/game-turn", consumes = "application/json", produces ="application/json")
	public PlayerMove launch(@RequestBody(required=false) GameRequest requestBody) {
		// check the request is just for test or for game beginning
		
		System.out.print(requestBody.toString());
		// after check request, analyse it
		NiebuszkowyManager nieBot = new NiebuszkowyManager();
		char myPlayer = requestBody.getVariables().getPlayer();
		System.out.print(requestBody);
		if (requestBody.getStatus() == Status.start) {
			nieBot.clearListPlayers();
		}
		Move myMove = nieBot.takeAction(requestBody.getBoard(), myPlayer, requestBody.getLastTurn());
		if (myMove == null ) {	// just fake data so fake response
			myMove = new Move(Action.Fire,Direction.DOWN);
		}
		if (myMove.getDirection() == null) {
			myMove.setDirection(Direction.NoDirection);
			if (myMove.getAction() == Action.Walk) {
				myMove.setAction(Action.Nothing);
			}
		}
		// if test - response with 
		//Move myMove = new Move(Action.Fire,Direction.DOWN);
		PlayerMove pMove = new PlayerMove(myMove);
		return pMove;
		//return new FairyGameResponse(pMove);
	}

}
