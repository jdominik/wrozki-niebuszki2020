package com.fairytales.wrozkiniebuszki2020.entity;

public class Board {
	private char[][] plan;
	
	public Board() {
		
	}

	public char[][] getPlan() {
		return plan;
	}

	public void setPlan(char[][] plan) {
		this.plan = plan;
	}
	
	
	
	/* "board": [ [ "string" ] ] */
}
