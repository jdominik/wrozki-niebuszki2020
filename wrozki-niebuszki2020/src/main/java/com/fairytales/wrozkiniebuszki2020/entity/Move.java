package com.fairytales.wrozkiniebuszki2020.entity;

public class Move {
	//private char charMove;
	private Action action;
	private Direction direction;
	
	public enum Action {
		Walk, Fire, Take, Freeze, Nothing, PostExit
	}
	
	public enum Direction {
		UP, DOWN, LEFT, RIGHT, NoDirection
	}
	
	public Move(Action theAction, Direction theDirection) {
		action = theAction;
		direction = theDirection;
	}

	/*
	 * public char getCharMove() { return charMove; }
	 * 
	 * public void setMyMove(char charMove) { this.charMove = charMove; }
	 */
	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result + ((direction == null) ? 0 : direction.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Move other = (Move) obj;
		if (action != other.action)
			return false;
		if (direction != other.direction)
			return false;
		return true;
	}
	
	
	
	
}
