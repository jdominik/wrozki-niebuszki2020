package com.fairytales.wrozkiniebuszki2020.manager;

import java.util.ArrayList;
import java.util.List;
import com.fairytales.wrozkiniebuszki2020.entity.BoardElement;
import com.fairytales.wrozkiniebuszki2020.entity.Move;
import com.fairytales.wrozkiniebuszki2020.entity.Move.Action;
import com.fairytales.wrozkiniebuszki2020.entity.Move.Direction;
import com.fairytales.wrozkiniebuszki2020.entity.Player;
import com.fairytales.wrozkiniebuszki2020.entity.Player.State;
import com.fairytales.wrozkiniebuszki2020.entity.Position;

public class NiebuszkowyManager {
	
	private static List<Player> playerList = new ArrayList();
	private static char[][] lastBoard;
	
	public void clearListPlayers() {
		playerList.clear();
		setLastBoard(null);
	}
	
    protected char[][] prepareLastBoard(char[][] board) {
    	int i;
    	int j;
    	//this.lastBoard = new char[board.length][board[0].length];
    	char[][] newBoard = new char[board.length][board[0].length];
    	for (i=0; i<board.length; i++) {
	    	for (j=0; j<board[0].length; j++) {
	    		newBoard[i][j] = board[i][j];
	    	}
	    }
    	return newBoard;
    }
	
	public NiebuszkowyManager() {
        
	}
	
	public Move takeAction(char[][] board, char myPlayerSign, String[] lastTurn) {
		
		List<Player> players = predictAction(board, myPlayerSign, lastTurn);
		Player myPlayer = players.stream().filter(Player::isMyPlayer).findFirst().orElse(null);
		if (myPlayer == null) return null;
		
		for (Player player : players) {
			
			Position nextPosOtherPlayer = predictNextPos(player);
			Direction shotDirection = null;
			if (player.getSign() != myPlayer.getSign()) {
				shotDirection = playerOnTheRoar(myPlayer, nextPosOtherPlayer, board);
			}	// TODO - else try to avoid the other player fire
			// check last move of my player:
			Move lastFireMove = null;
			if (lastTurn != null) {
				for (String playerTurn : lastTurn) {
					if(myPlayerSign == playerTurn.charAt(0)) {
						Direction takeDirection = null;
						if (playerTurn.charAt(1) == 'F') {
							char direct = playerTurn.charAt(2);
							if (direct == 'L') {
								takeDirection = Direction.LEFT;
							} else if(direct == 'R') {
								takeDirection = Direction.RIGHT;
							} else if(direct == 'U') {
								takeDirection = Direction.UP;
							} else if(direct == 'D') {
								takeDirection = Direction.DOWN;
							}
						}
						lastFireMove = new Move(Action.Fire, takeDirection);
						/*Position takeFrom = takeFromPos(player, takeMove);
						if(board[takeFrom.getX()][takeFrom.getY()] == '$') {
							if (player.getState() == Player.State.flag) {
								player.setState(Player.State.flag2);
							} else {
								player.setState(Player.State.flag);
							}
						}*/
						//if(playerOnTheRoar()	// get curr move of the player
						
					}
				}
			}
			if (shotDirection != null && (lastFireMove == null || !lastFireMove.equals(new Move(Action.Fire, shotDirection)))) {
				myPlayer.setNextMove(new Move(Action.Fire, shotDirection));
			}
			
		}
		
		return myPlayer.getNextMove();
	
	}
	
	private Direction playerOnTheRoar(Player myPlayer, Position nextPosOtherPlayer, char[][] board) {
		if ((myPlayer.getPosition().getX() == nextPosOtherPlayer.getX())) {
			
			if(myPlayer.getPosition().getY() < nextPosOtherPlayer.getY()) {
				int y = 0;
				for (y = myPlayer.getPosition().getY(); nextPosOtherPlayer.getY() != y; y++) {
					if (board[y][myPlayer.getPosition().getX()] == '#') {
						return null;
					}
				}
				return Direction.DOWN;
			} else {
				int y = 0;
				for (y = myPlayer.getPosition().getY(); nextPosOtherPlayer.getY() != y; y--) {
					if (board[y][myPlayer.getPosition().getX()] == '#') {
						return null;
					}
				}
				return Direction.UP;
			}
		} else if(myPlayer.getPosition().getY() == nextPosOtherPlayer.getY()) {
			
			if(myPlayer.getPosition().getX() < nextPosOtherPlayer.getX()) {
				int x = 0;
				for (x = myPlayer.getPosition().getX(); nextPosOtherPlayer.getX() != x; x++) {
					if (board[myPlayer.getPosition().getY()][x] == '#') {
						return null;						
					}
				}
				return Direction.RIGHT;
			} else {
				int x = 0;
				for (x = myPlayer.getPosition().getX(); nextPosOtherPlayer.getX() != x; x--) {
					if (board[myPlayer.getPosition().getY()][x] == '#') {
						return null;
					}
				}
				return Direction.LEFT;
			}
		}
		return null;
	}
	
	
	public List<Player> predictAction(char[][] board, char myPlayerSign, String[] lastTurn) {
		
		
		// A,B,C,D,@,$,#
		char[] boardElems = BoardElement.elementTypes;	//new String(BoardElement.elementTypes);	//Arrays.asList(BoardElement.elementTypes);
		char[] playerElems = BoardElement.playerTypes;	//new String(BoardElement.playerTypes);	//Arrays.asList(BoardElement.playerTypes);
		char[][] matrix = board;
		int w = matrix.length;
		int h = matrix[0].length;
		
		//List<Player> playerList = new ArrayList();
		playerList.stream().forEach(p-> p.setActive(false));	// initialization as not active at this turn, will be checked later
		
		for (int y = 0; y < w; ++y)
	    {
	        for (int x = 0; x < h; ++x)
	        {
	            if (contains(matrix[y][x],boardElems)) {
	            	
	                new BoardElement(matrix[y][x],new Position(x, y));
	            } else if (contains(matrix[y][x], playerElems)) {
	            	final char currSign = matrix[y][x];
	            	
            		Player playerOnList = playerList.stream().filter(p -> p.getSign() == currSign).findFirst().orElse(null);
            		if (playerOnList != null) {
            			playerOnList.setPosition(new Position(x, y));
            			playerOnList.setActive(true);
            		} else {
            			playerList.add(new Player(currSign, new Position(x, y), Player.State.normal, myPlayerSign == currSign));
	            	}
	            }
	        }
	    }
		
		playerList.removeIf(p -> !p.isActive()); // remove player from the list if no longer exists on the board
		
		char[][] newBoard = prepareLastBoard(board);
	
		
		char target;
		Player myPlayer = null;
		
		for (Player player : playerList) {
			if (lastTurn != null) {
				for (String playerTurn : lastTurn) {
					if(player.getSign() == playerTurn.charAt(0)) {
						Direction takeDirection = null;
						if (playerTurn.charAt(1) == 'T') {
							char direct = playerTurn.charAt(2);
							if (direct == 'L') {
								takeDirection = Direction.LEFT;
							} else if(direct == 'R') {
								takeDirection = Direction.RIGHT;
							} else if(direct == 'U') {
								takeDirection = Direction.UP;
							} else if(direct == 'D') {
								takeDirection = Direction.DOWN;
							}
						}
						Move takeMove = new Move(Action.Take, takeDirection);
						Position takeFrom = takeFromPos(player, takeMove);
						if (inRange(takeFrom.getY(), takeFrom.getX(), board)) {
							if ((lastBoard != null && lastBoard[takeFrom.getY()][takeFrom.getX()] == '$' || board[takeFrom.getY()][takeFrom.getX()] == '$')) {
								if (player.getState() == Player.State.flag) {
									player.setState(Player.State.flag2);
								} else {
									player.setState(Player.State.flag);
								}
							} else if (lastBoard != null && contains(lastBoard[takeFrom.getY()][takeFrom.getX()], playerElems)) {
								playerList.stream().filter(p -> p.getSign() == lastBoard[takeFrom.getY()][takeFrom.getX()] && (p.getState().equals(State.flag) || p.getState().equals(State.flag2))).forEach(p-> {
									if (player.getState() == Player.State.flag) {
										player.setState(Player.State.flag2);
									} else {
										player.setState(Player.State.flag);
									}
									if (p.getState() == Player.State.flag2) {
										p.setState(Player.State.flag);
									} else {
										p.setState(Player.State.normal);
									}
								});
							}
						}
								
						//if(playerOnTheRoar()	// get curr move of the player
						
					}
				}
			}
			
			
			
			player.setShortestPath(-1);
			
			if (player.getSign() == myPlayerSign) {
				player.setIsMyPlayer(true);
			}
			
			if (Player.State.flag.equals(player.getState()) || Player.State.flag2.equals(player.getState()) ) {
				target = player.getSign();
				for (Player otherPlayer : playerList) {
					if (player.getSign() == otherPlayer.getSign()) {
						target = '@';
						checkPath(otherPlayer, matrix, target);
						
						if (Player.State.flag.equals(player.getState())) {
							target = '$';
							checkPath(otherPlayer, matrix, target);
						}
					} else {
						int currPath = otherPlayer.getShortestPath();
						Move currMove = otherPlayer.getNextMove();
						target = player.getSign();
						checkPath(otherPlayer, matrix, target);
						// as other player can be hard to catch
						if (otherPlayer.getShortestPath() != -1 && currPath < otherPlayer.getShortestPath() * 2) {
							otherPlayer.setShortestPath(currPath);
							otherPlayer.setNextMove(currMove);
						}
					}
				}
							
			} else {
				target = '$';
				checkPath(player, matrix, target);
				if(player.getNextMove() == null) {	 // probably there is no flag on the board so just go away
					target = '@';
					checkPath(player, matrix, target);
				}
			}
			
		}
		lastBoard = newBoard;
		return playerList;
		//return myPlayer != null ? myPlayer.getNextMove() : null;
		
		
	}
	
	public void checkPath(Player player, char[][] matrix, char target) {
    	PathResolver path = new PathResolver(matrix);
        int shortestPath = path.solve(player.getPosition(), target);
        System.out.println("Solved: " + (shortestPath != -1));
        System.out.println(path.toString());
        if (shortestPath != -1 && (player.getShortestPath() == -1 || player.getShortestPath() > shortestPath )) {
        	player.setShortestPath(shortestPath);
        	Action action = null;
        	if (shortestPath == 1 && target != '@') {	// we cannot take exit, we need to get into it
        		action = Action.Take;
        	} else {
        		action = Action.Walk;
        	}
        	player.setNextMove(new Move(action, path.getNextMove(player.getPosition())));
        }
    }
	
	// this method can be called only after predict next move
	private Position predictNextPos(Player player) {
		Position currPos = player.getPosition();
		Position nextPos = currPos;
		
		Move predMove = player.getNextMove();
		if(predMove != null && predMove.getAction() != null && Action.Walk.equals(predMove.getAction())) {
			
			if (Direction.DOWN.equals(predMove.getDirection())) {
				nextPos.setY(currPos.getY() + 1);
			}
			else if (Direction.UP.equals(predMove.getDirection())) {
				nextPos.setY(currPos.getY() - 1);
			}
			else if (Direction.RIGHT.equals(predMove.getDirection())) {
				nextPos.setX(currPos.getX() + 1);
			}
			else if (Direction.LEFT.equals(predMove.getDirection())) {
				nextPos.setX(currPos.getX() - 1);
			}
		}
		
		return nextPos;
		
	}
	
	private Position takeFromPos(Player player, Move predMove) {
		Position currPos = player.getPosition();
		Position takePos = new Position(currPos.getX(),currPos.getY());
		if(predMove != null && predMove.getAction() != null && Action.Take.equals(predMove.getAction())) {
			
			if (Direction.DOWN.equals(predMove.getDirection())) {
				takePos.setY(currPos.getY() + 1);
			}
			else if (Direction.UP.equals(predMove.getDirection())) {
				takePos.setY(currPos.getY() - 1);
			}
			else if (Direction.RIGHT.equals(predMove.getDirection())) {
				takePos.setX(currPos.getX() + 1);
			}
			else if (Direction.LEFT.equals(predMove.getDirection())) {
				takePos.setX(currPos.getX() - 1);
			}
		}
		
		return takePos;
		
	}
	
	public boolean contains(char c, char[] array) {
        for (char x : array) {
            if (x == c) {
                return true;
            }
        }
        return false;
    }
	
	protected boolean inRange(int i, int j, char[][] map) {
        return inHeight(i, map) && inWidth(j, map);
    }

    private boolean inHeight(int i, char[][] map) {
        return i >= 0 && i < map.length;
    }

    private boolean inWidth(int j, char[][] map) {
        return j >= 0 && j < map[0].length;
    }

	public static char[][] getLastBoard() {
		return lastBoard;
	}

	public static void setLastBoard(char[][] lastBoard) {
		NiebuszkowyManager.lastBoard = lastBoard;
	}

		
}
