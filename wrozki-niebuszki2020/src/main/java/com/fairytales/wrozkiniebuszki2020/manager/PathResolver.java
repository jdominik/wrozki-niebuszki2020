package com.fairytales.wrozkiniebuszki2020.manager;

import java.util.Arrays;

import org.springframework.data.util.Pair;

import com.fairytales.wrozkiniebuszki2020.entity.Move;
import com.fairytales.wrozkiniebuszki2020.entity.Move.Direction;
import com.fairytales.wrozkiniebuszki2020.entity.Position;

public class PathResolver {


	    final static int TRIED = 2;
	    final static int TRIED_PATH = 4;
	    final static int PATH = 3;
	    final static char SHORTEST_PATH = '+';
	    
	    private int pathLen;
	    private int minPathLen;
	    private int levelOfTrav;
	    private boolean lastTry;


	    // @formatter:off
	    private static int[][] GRID = { 
	        { 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1 },
	        { 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1 },
	        { 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0 },
	        { 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1 },
	        { 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1 },
	        { 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1 },
	        { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 } 
	    };
	    // @formatter:off

	    

	    private char[][] grid;
	    private int height;
	    private int width;

	    private char[][] map;
	    private char[][] shortestMap;

	    public PathResolver(char[][] matrix) {
	    	grid = matrix.clone();
	        this.grid = grid;
	        this.height = grid.length;
	        this.width = grid[0].length;

	        this.map = new char[height][width];
	        this.shortestMap = new char[height][width];
	        this.pathLen = 0;
	        this.minPathLen = -1;
	        this.levelOfTrav = 0;
	        this.lastTry = false;
	    }
	    
	    protected void prepareShortestTry() {
	    	int i;
	    	int j;
	    	for (i=0; i<shortestMap.length; i++) {
		    	for (j=0; j<shortestMap[0].length; j++) {
		    		if (map[i][j] == PATH) {
		    			shortestMap[i][j] = SHORTEST_PATH;
		    		} else {
		    			shortestMap[i][j] = ' ';
		    		}
		    	}
		    }
	    }
	    
	    protected void prepareNextTry() {
	    	int i;
	    	int j;
	    	for (i=0; i<map.length; i++) {
		    	for (j=0; j<map[0].length; j++) {
		    		if (map[i][j] == PATH) {
		    			map[i][j] = TRIED_PATH;
		    		}
		    	}
		    }
	    }
	    
	    protected Position checkTargetPos(char target) {
	    	int i;
	    	int j;
	    	for (i=0; i<grid.length; i++) {
		    	for (j=0; j<grid[0].length; j++) {
		    		if (grid[i][j] == target) {
		    			return new Position(j,i);
		    		}
		    	}
		    }
	    	return null;
	    }
	    

	    public int solve(Position pos, char target) {
	    	Position targetPos = checkTargetPos(target);
	    	Pair<Direction,Direction> directionToTarget = null;
	    	if (targetPos != null) {
		    	if ((pos.getY()) < targetPos.getY()) {
					if (pos.getX() < targetPos.getX()) {
						directionToTarget = Pair.of(Direction.DOWN, Direction.RIGHT);
					} else {
						directionToTarget= Pair.of(Direction.DOWN, Direction.LEFT);
					}
				} else if (pos.getY() == targetPos.getY()) {
					if(pos.getX() < targetPos.getX()) {
						directionToTarget = Pair.of(Direction.NoDirection, Direction.RIGHT);
					} else {
						directionToTarget = Pair.of(Direction.NoDirection, Direction.LEFT);
					}
				} else {
					if(pos.getX() < targetPos.getX()) {
						directionToTarget = Pair.of(Direction.UP, Direction.RIGHT);
					} else {
						directionToTarget = Pair.of(Direction.UP, Direction.LEFT);
					}
				}
		    	
		    	
		    	grid[pos.getY()][pos.getX()] = ' ';
		    	//int pathLen = 0;
		         while (lastTry == false) {
			        pathLen = 0;
			        levelOfTrav = 0;
			        prepareNextTry();
			        map[pos.getY()][pos.getX()] = ' ';
			        int currLen = traverse(pos.getY(), pos.getX(), target, directionToTarget);
			        if (minPathLen== -1 || (currLen!=-1 && currLen < minPathLen)) {
			        	minPathLen = currLen;
			        	prepareShortestTry();
			        }
		         }
	    	}
	         return minPathLen;
	    }

	    private int traverse(int i, int j, char target, Pair<Direction,Direction> directionToTarget) {
	    	if (Direction.DOWN.equals(directionToTarget.getFirst())) {
	    		if (Direction.RIGHT.equals(directionToTarget.getSecond())) {
	    			return traverseSE(i, j, target);
	    		} else {
	    			return traverseSW(i, j, target);
	    		}
	    	} else if (Direction.NoDirection.equals(directionToTarget.getFirst())) {
	    		if (Direction.RIGHT.equals(directionToTarget.getSecond())) {
	    			return traverseE(i, j, target);
	    		} else {
	    			return traverseW(i, j, target);
	    		}
	    	} else {
	    		if (Direction.RIGHT.equals(directionToTarget.getSecond())) {
	    			return traverseNE(i, j, target);
	    		} else {
	    			return traverseNW(i, j, target);
	    		}
	    	}

	        
	        
	    }

	    private boolean isEnd(char current, char target) {
	        return current == target;
	    }

	    private boolean isValid(int i, int j, char target) {
	        if (inRange(i, j) && isOpen(i, j, target) && !isTried(i, j)) {
	            return true;
	        }

	        return false;
	    }

	    private boolean isOpen(int i, int j, char target) {
	        return (grid[i][j] == ' ' || grid[i][j] == '$' || grid[i][j] == target || grid[i][j] == '4');
	    }

	    private boolean isTried(int i, int j) {
	        return map[i][j] == TRIED;
	    }

	    private boolean inRange(int i, int j) {
	        return inHeight(i) && inWidth(j);
	    }

	    private boolean inHeight(int i) {
	        return i >= 0 && i < height;
	    }

	    private boolean inWidth(int j) {
	        return j >= 0 && j < width;
	    }

	    public String toString() {
	        String s = "";
	        for (char[] row : shortestMap) {
	            s += Arrays.toString(row) + "\n";
	        }

	        return s;
	    }
	    
	    public Move.Direction getNextMove(Position playerPos) {
	    	int i = playerPos.getY();
	    	int j = playerPos.getX();
	    	
	    	if (inRange(i-1, j) && shortestMap[i-1][j] == SHORTEST_PATH) {
	            
	            return Move.Direction.UP;
	        }
	        // East
	        if (inRange(i, j+1) && shortestMap[i][j+1] == SHORTEST_PATH) {
	        	return Move.Direction.RIGHT;
	        }
	        // South
	        if (inRange(i+1, j) && shortestMap[i+1][j] == SHORTEST_PATH) {
	        	return Move.Direction.DOWN;
	        }
	        // West
	        if (inRange(i, j-1) && shortestMap[i][j-1] == SHORTEST_PATH) {
	        	return Move.Direction.LEFT;
	        }
	        return null;
	    	
	    }
	    
	    private int traverseNE(int i, int j, char target) {
	    	levelOfTrav++;
	    	
	        if (!isValid(i,j,target)) {
	        	levelOfTrav--;
	            return -1;
	        }
	        if (minPathLen != -1 && levelOfTrav > minPathLen) {
	        	levelOfTrav--;
	        	return -1;
	        }

	        if ( isEnd(grid[i][j], target) ) {
	            return pathLen;
	        } else {
	            map[i][j] = TRIED;
	        }
	    	
	    	// North
	        if (traverseNE(i - 1, j, target) != -1) {
	            map[i-1][j] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            return pathLen;
	        }
	        // East
	        if (traverseNE(i, j + 1, target) != -1) {
	            map[i][j + 1] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            return pathLen;
	        }
	        // South
	        if (traverseNE(i + 1, j, target) != -1) {
	            map[i + 1][j] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            return pathLen;
	        }
	        // West
	        if (traverseNE(i, j - 1, target) != -1) {
	            map[i][j - 1] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            if (levelOfTrav <= 1) {
	            	lastTry = true;
	            }
	            return pathLen;
	        }
	        levelOfTrav--;
	        if (levelOfTrav <= 1) {
            	lastTry = true;
            }
	    
	        return -1;
	    }
	    private int traverseNW(int i, int j, char target) {
	    	levelOfTrav++;
	    	
	        if (!isValid(i,j,target)) {
	        	levelOfTrav--;
	            return -1;
	        }
	        if (minPathLen != -1 && levelOfTrav > minPathLen) {
	        	levelOfTrav--;
	        	return -1;
	        }

	        if ( isEnd(grid[i][j], target) ) {
	            return pathLen;
	        } else {
	            map[i][j] = TRIED;
	        }
	    	
	    	// North
	        if (traverseNW(i - 1, j, target) != -1) {
	            map[i-1][j] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            return pathLen;
	        }
	     // West
	        if (traverseNW(i, j - 1, target) != -1) {
	            map[i][j - 1] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            
	            return pathLen;
	        }
	        
	        // South
	        if (traverseNW(i + 1, j, target) != -1) {
	            map[i + 1][j] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            return pathLen;
	        }
	     // East
	        if (traverseNW(i, j + 1, target) != -1) {
	            map[i][j + 1] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            if (levelOfTrav <= 1) {
	            	lastTry = true;
	            }
	            return pathLen;
	        }
	        
	        levelOfTrav--;
	        if (levelOfTrav <= 1) {
            	lastTry = true;
            }
	    
	        return -1;
	    }
	    
	    private int traverseSW(int i, int j, char target) {
	    	levelOfTrav++;
	    	
	        if (!isValid(i,j,target)) {
	        	levelOfTrav--;
	            return -1;
	        }
	        if (minPathLen != -1 && levelOfTrav > minPathLen) {
	        	levelOfTrav--;
	        	return -1;
	        }

	        if ( isEnd(grid[i][j], target) ) {
	            return pathLen;
	        } else {
	            map[i][j] = TRIED;
	        }
	    	
	    	
	        // South
	        if (traverseSW(i + 1, j, target) != -1) {
	            map[i + 1][j] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            return pathLen;
	        }
	        // West
	        if (traverseSW(i, j - 1, target) != -1) {
	            map[i][j - 1] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            
	            return pathLen;
	        }
	     // North
	        if (traverseSW(i - 1, j, target) != -1) {
	            map[i-1][j] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            return pathLen;
	        }
	        // East
	        if (traverseSW(i, j + 1, target) != -1) {
	            map[i][j + 1] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            if (levelOfTrav <= 1) {
	            	lastTry = true;
	            }
	            return pathLen;
	        }
	        levelOfTrav--;
	        if (levelOfTrav <= 1) {
            	lastTry = true;
            }
	    
	        return -1;
	    }
	    
	    private int traverseSE(int i, int j, char target) {
	    	levelOfTrav++;
	    	
	        if (!isValid(i,j,target)) {
	        	levelOfTrav--;
	            return -1;
	        }
	        if (minPathLen != -1 && levelOfTrav > minPathLen) {
	        	levelOfTrav--;
	        	return -1;
	        }

	        if ( isEnd(grid[i][j], target) ) {
	            return pathLen;
	        } else {
	            map[i][j] = TRIED;
	        }
	    	
	    	
	        // South
	        if (traverseSE(i + 1, j, target) != -1) {
	            map[i + 1][j] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            return pathLen;
	        }
	     // East
	        if (traverseSE(i, j + 1, target) != -1) {
	            map[i][j + 1] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            
	            return pathLen;
	        }
	        // West
	        if (traverseSE(i, j - 1, target) != -1) {
	            map[i][j - 1] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            
	            return pathLen;
	        }
	     // North
	        if (traverseSE(i - 1, j, target) != -1) {
	            map[i-1][j] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            if (levelOfTrav <= 1) {
	            	lastTry = true;
	            }
	            return pathLen;
	        }
	        
	        levelOfTrav--;
	        if (levelOfTrav <= 1) {
            	lastTry = true;
            }
	    
	        return -1;
	    }
	    
	    private int traverseW(int i, int j, char target) {
	    	levelOfTrav++;
	    	
	        if (!isValid(i,j,target)) {
	        	levelOfTrav--;
	            return -1;
	        }
	        if (minPathLen != -1 && levelOfTrav > minPathLen) {
	        	levelOfTrav--;
	        	return -1;
	        }

	        if ( isEnd(grid[i][j], target) ) {
	            return pathLen;
	        } else {
	            map[i][j] = TRIED;
	        }
	    	
	     // West
	        if (traverseW(i, j - 1, target) != -1) {
	            map[i][j - 1] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            
	            return pathLen;
	        }
	    	
	        // South
	        if (traverseW(i + 1, j, target) != -1) {
	            map[i + 1][j] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            return pathLen;
	        }
	     
	        
	     // North
	        if (traverseW(i - 1, j, target) != -1) {
	            map[i-1][j] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            
	            return pathLen;
	        }
	        
	     // East
	        if (traverseW(i, j + 1, target) != -1) {
	            map[i][j + 1] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            if (levelOfTrav <= 1) {
	            	lastTry = true;
	            }
	            return pathLen;
	        }
	        
	        levelOfTrav--;
	        if (levelOfTrav <= 1) {
            	lastTry = true;
            }
	    
	        return -1;
	    }
	    
	    private int traverseE(int i, int j, char target) {
	    	levelOfTrav++;
	    	
	        if (!isValid(i,j,target)) {
	        	levelOfTrav--;
	            return -1;
	        }
	        if (minPathLen != -1 && levelOfTrav > minPathLen) {
	        	levelOfTrav--;
	        	return -1;
	        }

	        if ( isEnd(grid[i][j], target) ) {
	            return pathLen;
	        } else {
	            map[i][j] = TRIED;
	        }
	    	
	    	
	        
	     // East
	        if (traverseE(i, j + 1, target) != -1) {
	            map[i][j + 1] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            
	            return pathLen;
	        }
	     // South
	        if (traverseE(i + 1, j, target) != -1) {
	            map[i + 1][j] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            return pathLen;
	        }
	     // North
	        if (traverseE(i - 1, j, target) != -1) {
	            map[i-1][j] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            
	            return pathLen;
	        }
	        // West
	        if (traverseE(i, j - 1, target) != -1) {
	            map[i][j - 1] = PATH;
	            pathLen++;
	            levelOfTrav--;
	            if (levelOfTrav <= 1) {
	            	lastTry = true;
	            }
	            return pathLen;
	        }
	     
	        
	        levelOfTrav--;
	        if (levelOfTrav <= 1) {
            	lastTry = true;
            }
	    
	        return -1;
	    }
	    	

	

}
